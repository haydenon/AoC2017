def ascii_list(values, suffix_lengths)
  values.split("").map { |c| c.ord }.concat(suffix_lengths)
end

input = "94,84,0,79,2,27,81,1,123,93,218,23,103,255,254,243"
suffix = [17, 31, 73, 47, 23]
lengths = ascii_list(input, [17, 31, 73, 47, 23])
list = (0..255).to_a

def knot(list, first, length)
  rotated = list.rotate(first)
  rotated[0...length] = rotated[0...length].reverse
  rotated.rotate(-first)
end

def knot_round(list, lengths, position, skip)
  current_position = position
  skip_size = skip
  current_list = list
  lengths.each do |length|
    current_list = knot(current_list, current_position, length)
    current_position += (length + skip_size) % list.length
    skip_size += 1
  end
  [current_list, current_position, skip_size]
end

def hash(list, lengths)
  hash_list = list
  pos, skip = [0, 0]
  (1..64).each do
    hash_list, pos, skip = knot_round(hash_list, lengths, pos, skip)
  end
  dense_hash = []
  (0...16).each do |num|
    start = (num * 16)
    dense_hash << hash_list[start...(start + 16)].inject(:^)
  end
  dense_hash.map{|num| num.to_s(16).rjust(2, "0")}.join()
end

result = hash(list, lengths)
puts result
