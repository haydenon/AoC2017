require ('set')

def hash_value(values)
  hash = 2166136261
  values.inject(hash) { |sum, val|
    ((sum * 16777619) ^ val) % 2 ** 32
  }
end

def parse(input)
    input.split().map { |val| val.to_i }
end

def redistribute(values)
    set = Set.new()
    set << hash_value(values)
    steps = 0
    first_duplicate = nil
    while true do
        steps += 1
        max = values.max
        index = values.find_index max
        values [index] = 0
        while max > 0 do
            index = (index + 1) % values.length
            values[index] += 1
            max -= 1
        end
        current_length = set.length
        new_hash = hash_value(values)
        break if first_duplicate == new_hash
        if first_duplicate.nil? then
            set << new_hash
            if current_length == set.length then
                first_duplicate = new_hash
                steps = 0
            end
        end
    end
    steps
end

print (redistribute(parse "0	5	10	0	11	14	13	4	11	8	8	7	1	4	12	11"))