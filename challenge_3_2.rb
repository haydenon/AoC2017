class Grid
  def initialize(size)
    @size = size
    @array = Array.new(size) { Array.new(size) }
  end

  def get(x, y)
    @array[x + @size / 2][y + @size / 2]
  end

  def set(x, y, value)
    @array[x + @size / 2][y + @size / 2] = value
  end

  def surrounding_values(x, y)
    x_pos = x + @size / 2
    y_pos = y + @size / 2
    (-1..1).flat_map { |col|
      (-1..1).map { |row|
        @array[x_pos + col][y_pos + row]
      }
    }.select { |val| not val.nil? }
  end

  def print_grid
    print @array
  end
end

def next_pos(grid, dir, x, y)
  case dir
  when :right
    if grid.get(x, y - 1).nil?
      [:up, x, y - 1]
    else
      [:right, x + 1, y]
    end
  when :up
    if grid.get(x - 1, y).nil?
      [:left, x - 1, y]
    else
      [:up, x, y - 1]
    end
  when :left
    if grid.get(x, y + 1).nil?
      [:down, x, y + 1]
    else
      [:left, x - 1, y]
    end
  else
    if grid.get(x + 1, y).nil?
      [:right, x + 1, y]
    else
      [:down, x, y + 1]
    end
  end
end

def greater_than(number)
  grid = Grid.new(50)

  x, y = [1, 0]
  grid.set(0, 0, 1)
  grid.set(1, 0, 1)
  dir = :right
  while grid.get(x, y) < number
    dir, x, y = next_pos(grid, dir, x, y)
    grid.set(x, y, grid.surrounding_values(x, y).inject(:+))
  end
  grid.get(x, y)
end

print (greater_than 265149)
