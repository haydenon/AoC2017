def steps(number)
  return 0 if number == 1

  square_value = (3..Float::INFINITY).step(2).find { |val|
    int = val.to_i
    (int ** 2) >= number
  }.to_i

  lower_range = (square_value - 2) ** 2
  upper_value = square_value ** 2

  min = (square_value / 2).floor
  chunk = (upper_value - lower_range) / 4
  steps_to_next = min - ((number - lower_range) % chunk)

  steps_to_next.abs + (square_value - 1) / 2
end

print (steps 265149)
