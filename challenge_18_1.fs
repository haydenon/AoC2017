module Challenge18p1

type Value =
    | Lit of int64
    | Var of string

type Instruction = 
    | Snd of string
    | Set of (string * Value)
    | Add of (string * Value)
    | Mul of (string * Value)
    | Mod of (string * Value)
    | Rcv of string
    | Jgz of (Value * Value)

let [<Literal>] SoundReg= "_sound"

let reg (registers : Map<string, int64>) key =
    match registers.TryFind key with
    | Some v -> v
    | None   -> 0L

let hasKey (registers : Map<string, int64>) key =
    registers.ContainsKey key

let value (registers : Map<string, int64>) item =
    match item with
    | Lit v -> v
    | Var key -> reg registers key

let modify (transformation: int64 -> int64 -> int64) (registers : Map<string,int64>) key item =
    value registers item
    |> (fun y -> transformation (reg registers key) y)
    |> fun int -> registers.Add(key, int)

let set = modify (fun _ y -> y)
let add = modify (+)
let mul = modify (*)
let modu = modify (%)

let play instructions =
    let rec playInstructions (instructions : Instruction []) pos (regs : Map<string, int64>) =
        let length = Array.length instructions
        if pos >= length || pos < 0
        then
            reg regs SoundReg
        else
            let ins = instructions.[pos]
            match ins with
            | Snd key ->
                set regs SoundReg (Var key)
                |> playInstructions instructions (pos + 1) 
            | Set (key, item) ->
                set regs key item
                |> playInstructions instructions (pos + 1)
            | Add (key, item) ->
                add regs key item
                |> playInstructions instructions (pos + 1)
            | Mul (key, item) ->
                mul regs key item
                |> playInstructions instructions (pos + 1)
            | Mod (key, item) ->
                modu regs key item
                |> playInstructions instructions (pos + 1)
            | Rcv key ->
                match reg regs key with
                | 0L -> playInstructions instructions (pos + 1) regs
                | _ -> reg regs SoundReg
            | Jgz (cond, jump) ->
                match value regs cond with
                | 0L -> playInstructions instructions (pos + 1) regs
                | _ -> playInstructions instructions (pos + int32(value regs jump)) regs
    playInstructions instructions 0 Map.empty<string, int64>    

let (|Prefix|_|) (p:string) (s:string) =
    if s.StartsWith(p) then
        Some(s.Substring(p.Length + 1).Split(' '))
    else
        None
    
let getValue str =
    match System.Int64.TryParse(str) with
    | (true, int) -> Lit int
    | _ -> Var str

let instruction str =
    match str with
    | Prefix "snd" x -> Snd x.[0]
    | Prefix "set" x -> Set (x.[0], getValue x.[1])
    | Prefix "add" x -> Add (x.[0], getValue x.[1])
    | Prefix "mul" x -> Mul (x.[0], getValue x.[1])
    | Prefix "mod" x -> Mod (x.[0], getValue x.[1])
    | Prefix "rcv" x -> Rcv x.[0]
    | Prefix "jgz" x -> Jgz (getValue x.[0], getValue x.[1])
    | _ -> failwith "Invalid input"

let parse (input : string) =
    input.Split("\n")
    |> Array.map instruction

let instructions = parse """set i 31
set a 1
mul p 17
jgz p p
mul a 2
add i -1
jgz i -2
add a -1
set i 127
set p 826
mul p 8505
mod p a
mul p 129749
add p 12345
mod p a
set b p
mod b 10000
snd b
add i -1
jgz i -9
jgz a 3
rcv b
jgz b -1
set f 0
set i 126
rcv a
rcv b
set p a
mul p -1
add p b
jgz p 4
snd a
set a b
jgz 1 3
snd b
set f 1
add i -1
jgz i -11
snd a
jgz f -16
jgz a -19"""