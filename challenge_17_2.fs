module Challenge17_2

let afterZero times skip =
    let mutable pos = 0
    let mutable number = 0
    for i = 1 to times do
        pos <- ((pos + skip) % i + 1)
        if pos = 1 then number <- i
    number
    