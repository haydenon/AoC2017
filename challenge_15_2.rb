class Generator
    def initialize(factor, start, divider, multiple)
        @factor = factor
        @previous = start
        @divider = divider
        @multiple = multiple
    end

    def generate_next()
        val = (@previous * @factor) % @divider
        while val % @multiple != 0
          @previous = val
          val = (@previous * @factor) % @divider 
        end
        @previous = val
        val
    end
end

gen_a = Generator.new(16807, 703, 2147483647, 4)
gen_b = Generator.new(48271, 516, 2147483647, 8)
generators = [gen_a, gen_b]

def last_bits(val)
    binary = val.to_s(2).rjust(16, '0')
    binary[(binary.length - 16)..(binary.length - 1)]
end

def count_matches(generators)
    matches = 0
    (1..5000000).each do |round|
        values = generators.map{ |gen| gen.generate_next }
        first_val = last_bits values[0]
        matches += 1 if values.all? { |val| last_bits(val) == first_val }
    end
    matches
end

puts count_matches(generators)