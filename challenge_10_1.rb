list = (0..255).to_a
lengths = "94,84,0,79,2,27,81,1,123,93,218,23,103,255,254,243".split(",").map { |num| num.to_i }

def knot(list, first, length)
  rotated = list.rotate(first)
  rotated[0...length] = rotated[0...length].reverse
  rotated.rotate(-first)
end

def tie_knots(list, lengths)
  current_position = 0
  skip_size = 0
  current_list = list
  lengths.each do |length|
    current_list = knot(current_list, current_position, length)
    current_position += (length + skip_size) % list.length
    skip_size += 1
  end
  current_list
end

result = tie_knots(list, lengths)
puts result[0] * result[1]