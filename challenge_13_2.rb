input = "0: 4
1: 2
2: 3
4: 4
6: 6
8: 5
10: 6
12: 6
14: 6
16: 12
18: 8
20: 9
22: 8
24: 8
26: 8
28: 8
30: 12
32: 10
34: 8
36: 12
38: 10
40: 12
42: 12
44: 12
46: 12
48: 12
50: 14
52: 14
54: 12
56: 12
58: 14
60: 14
62: 14
66: 14
68: 14
70: 14
72: 14
74: 14
78: 18
80: 14
82: 14
88: 18
92: 17"

def parse(str)
    rows = str.split("\n")
    rows.map { |row| row.split(": ").map{ |val| val.to_i }}.to_h
end

def get_severity(depth_to_range, start)
    caught = false
    (0..depth_to_range.keys.max).each do |index|
        range = depth_to_range[index] 
        next if range.nil?
        length = range * 2 - 2
        caught = true if (index + start) % length == 0
    end
    caught
end

def zero_severtity(depth_to_range)
    (0..Float::INFINITY).each do |start|
        return start.to_i if not get_severity(depth_to_range, start.to_i)
    end
end

depth_to_range = parse(input)
puts zero_severtity(depth_to_range)
