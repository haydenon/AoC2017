module Challenge17_1

let insert ls i x =
    match i with
    | 0 -> x :: ls
    | _ -> List.splitAt i ls |> fun (a,b) -> List.append a (x :: b)

let generateBuffer times skip =
    let _, buffer =
        List.fold (fun acc index -> 
            let pos, ls = acc
            let insertIndex = ((pos + skip) % (List.length ls) + 1)
            (insertIndex, (insert ls insertIndex (index + 1)))
        ) (0, [0]) [0..(times - 1)]
    buffer