input = "ugkiagan"

def ascii_list(values, suffix_lengths)
  values.split("").map { |c| c.ord }.concat(suffix_lengths)
end

suffix = [17, 31, 73, 47, 23]
list = (0..255).to_a

def knot(list, first, length)
  rotated = list.rotate(first)
  rotated[0...length] = rotated[0...length].reverse
  rotated.rotate(-first)
end

def knot_round(list, lengths, position, skip)
  current_position = position
  skip_size = skip
  current_list = list
  lengths.each do |length|
    current_list = knot(current_list, current_position, length)
    current_position += (length + skip_size) % list.length
    skip_size += 1
  end
  [current_list, current_position, skip_size]
end

def get_hash(lengths)
  list = (0..255).to_a
  hash_list = list
  pos, skip = [0, 0]
  (1..64).each do
    hash_list, pos, skip = knot_round(hash_list, lengths, pos, skip)
  end
  dense_hash = []
  (0...16).each do |num|
    start = (num * 16)
    dense_hash << hash_list[start...(start + 16)].inject(:^)
  end
  dense_hash.map { |num| num.to_s(16).rjust(2, "0") }.join()
end

grid = (0..127).map do |row|
  row_input = "#{input}-#{row}"
  lengths = ascii_list(row_input, suffix)
  hash_val = get_hash(lengths)
  binary = hash_val.split("").map { |char| char.hex.to_s(2).rjust(4, '0') }.join
  binary.split("").map { |digit| digit == '1' }
end

filled = grid.inject(0) do |sum, row|
  sum += row.inject(0) { |sum, val| sum += if val then 1 else 0 end }
end

puts "#{filled}"
